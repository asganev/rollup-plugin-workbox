# rollup-plugin-workbox

[![Published on npm](https://img.shields.io/npm/v/rollup-plugin-workbox.svg)](https://www.npmjs.com/package/rollup-plugin-workbox)

Rollup plugin that builds a service worker with workbox as part of your rollup build

## Usage

`workbox` (the rollup plugin function) takes two objects as arguments, the first is an options object, the second is the [workbox config](https://developers.google.com/web/tools/workbox/guides/generate-service-worker/cli), like you would use with workbox cli. You can choose to specify the workbox config directly in your `rollup.config.js`, or `require` it from your `workbox-config.js`.

```js
import workbox from 'rollup-plugin-workbox'

export default {
  input: /*...*/,
  output: /*...*/,
  plugins: [
    workbox({
      /** @type {'generateSW'|'injectManifest'} default 'generateSW' */
      mode: 'injectManifest'
      /** @type {Function} default fancy render */
      render: ({ swDest, count, size }) => console.log(
        '📦', swDest,
        '#️⃣', count,
        '🐘', size,
      ),
      /** @type {Object} no default */
      workboxConfig: require('./workbox-config'),
    }
  ],
}
```
