import { generateSW, injectManifest } from 'workbox-build';
import colors from 'colors';
import boxen from 'boxen';
import filesize from 'filesize';

/** @enum {Function} Workbox mode */
const WORKBOX_MODE = {
  /** Inject a precache manifest into an existing service worker script. */
  injectManifest,
  /** Generate a new service worker script. */
  generateSW,
};

/** @typedef {'injectManifest'|'generateSW'} workboxMode */

const name = 'workbox';

const report = ({ swDest, count, size }) =>
  console.log(boxen(`
    ${colors.green.bold('Service Worker Built: ')} ${colors.yellow(swDest)}
    ${colors.green.bold('Cache size: ')} ${colors.yellow(filesize(size))}\
    ${colors.green.bold('Files Precached: ')} ${colors.yellow(count)}
  `, { padding: 1 }
  ));

/**
 * Build me a service-worker
 * @param  {Object}       options
 * @param  {workboxMode}  options.mode                   Whether to generate a service worker directly from the config or inject a precache manifest into an existing service worker.
 * @param  {Function}     options.render                 Function which renders output when service worker is built.
 * @param  {Object}       options.workboxConfig          The workbox configuration object.
 * @param  {String}       options.workboxConfig.swSrc    Service worker source file (for use with injectManifest).
 * @param  {String}       options.workboxConfig.swDest   Service worker destination file.
 * @return {Promise}
 */
export default function workbox({ workboxConfig, render = report, mode = 'generateSW' }) {
  const { swDest } = workboxConfig;

  if (!swDest) throw new Error('No service worker destination specified');

  if (!(mode in WORKBOX_MODE)) {
    throw new Error('Must choose one of injectManifest or generateSW modes');
  }

  const build = WORKBOX_MODE[mode];

  const doRender = ({ count, size }) =>
    render({ swDest, count, size });

  return {
    name,
    generateBundle(outputOptions, bundle, isWrite) {
      return build(workboxConfig)
        .then(doRender)
        .catch(console.error);
    },
  };
}
